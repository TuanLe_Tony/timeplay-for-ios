
# TIME PLAY

Sample code for a time play app. Written in Swift + Firebase.

## Software Requirements - Scenario 
###  The application has 2 options: Players & View Play
#### Players: selecting room for playing
#### View Play: Viewing the questions and Answers.
##### The app can to have many Players in the same room
Show name of players.
Show name of room.
Show count down time for waiting players.
##### The app can to have many View Play room for mutilple players


# 
# Selecting Room (Players Screen) and Setting Room (View Play)

![Scheme](IOS-images/First.jpg)

View play creates room
Players Screen selects room and input player's name.

# Players Screen - View Screen
![Scheme](IOS-images/room.jpg)

Show Play can show mutiples players's name

# Players Screen - View Screen
## Waiting for Questions and Answers
![Scheme](IOS-images/load.jpg)

Take few moments to loading, after that questions will show and count down for each question will appear. Then, you have 10 minutes to choose your answer.
# Players Screen - View Screen
## Showing Answer options and Question 1
![Scheme](IOS-images/question1.jpg)

# Players Screen - View Screen
## Showing Answer selected and Question 1
![Scheme](IOS-images/answer1.jpg)
wrong answer score : 0

# Players Screen - View Screen
## Showing Answer selected for question 2
![Scheme](IOS-images/rightanwser2.jpg)
wrong answer score : 1

# Players Screen - View Screen
## Showing Answers and question 3
![Scheme](IOS-images/question3.jpg)

# Players Screen - View Screen
## Showing Answer selected for question 3
![Scheme](IOS-images/rightanswer3.jpg)
wrong answer score : 2


# KEEP DOING THIS UNTIL ALL QUESTIONS HAVE SHOWED.

## Result :

# Players Screen - View Screen
## Game completted and Loading result.
![Scheme](IOS-images/result.jpg)


# Players Screen - View Screen
## Final result : Will show a name of person who has the hightest score.
![Scheme](IOS-images/getresult.jpg)











